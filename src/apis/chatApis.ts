import Dispatch from 'apis/client';

const urls = {
  getAllChats: (method: string) => {
    return {
      url: '/',
      method,
    };
  },
  postNewChat: (method: string) => {
    return {
      url: '',
      method,
    };
  },
};

function api() {
  return {
    getAllChats: (data) => Dispatch(urls.getAllChats('get'), data.params, data.payload),
    postNewChat: (data) => Dispatch(urls.getAllChats('post'), {}, data.payload),
  };
}

export default api();
