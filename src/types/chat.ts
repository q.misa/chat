export type IChat = {
  author: string;
  message: string;
  createdAt: number;
  owner: boolean;
};
