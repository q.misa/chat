import { IChat } from 'types/chat';

export enum ChatAction {
  GET_ALL_CHATS_REQUEST = '@@chat/GET_ALL_CHATS_REQUEST',
  GET_ALL_CHATS_SUCCESS = '@@chat/GET_ALL_CHATS_SUCCESS',
  GET_ALL_CHATS_ERROR = '@@chat/GET_ALL_CHATS_ERROR',
  POST_NEW_CHAT_REQUEST = '@@chat/POST_NEW_CHAT_REQUEST',
  POST_NEW_CHAT_SUCCESS = '@@chat/POST_NEW_CHAT_SUCCESS',
  POST_NEW_CHAT_ERROR = '@@chat/POST_NEW_CHAT_ERROR',
}

export type Action =
  | {
      type: ChatAction.GET_ALL_CHATS_REQUEST;
      payload: {};
    }
  | { type: ChatAction.GET_ALL_CHATS_SUCCESS; payload: IChat[] }
  | {
      type: ChatAction.GET_ALL_CHATS_ERROR;
      payload: any;
    }
  | {
      type: ChatAction.POST_NEW_CHAT_REQUEST;
      payload: IChat;
    }
  | {
      type: ChatAction.POST_NEW_CHAT_SUCCESS;
      payload: IChat;
    }
  | {
      type: ChatAction.POST_NEW_CHAT_ERROR;
      payload: any;
    };

export const fetchAllChatsRequest = (payload: any): Action => {
  return { type: ChatAction.GET_ALL_CHATS_REQUEST, payload };
};

export const fetchAllChatsSuccess = (payload: IChat[]): Action => {
  return { type: ChatAction.GET_ALL_CHATS_SUCCESS, payload };
};

export const fetchAllChatsError = (payload: any): Action => {
  return { type: ChatAction.GET_ALL_CHATS_ERROR, payload };
};
export const postNewMessageRequest = (payload: IChat): Action => {
  return { type: ChatAction.POST_NEW_CHAT_REQUEST, payload };
};

export const postNewMessageSuccess = (payload: IChat): Action => {
  return { type: ChatAction.POST_NEW_CHAT_SUCCESS, payload };
};

export const postNewMessageError = (payload: any): Action => {
  return { type: ChatAction.POST_NEW_CHAT_ERROR, payload };
};
