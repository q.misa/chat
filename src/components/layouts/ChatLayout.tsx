import React from 'react';
import ChatList from 'components/chats/ChatList';
import NewChat from 'components/chats/NewChat';
import { initialState, since, limit, intervalTime, reducer } from 'reducers/chatReducer';
import chatApis from 'apis/chatApis';
import { token } from 'apis/client';
import { IChat } from 'types/chat';
import { fetchAllChatsError, fetchAllChatsSuccess, postNewMessageSuccess } from 'actions/chatActions';
import 'styles/chatLayout.css';

const ChatLayout = (): JSX.Element => {
  const [state, dispatch] = React.useReducer(reducer, initialState);

  const fetchAllChats = (params: {}) => {
    chatApis
      .getAllChats({ payload: {}, params })
      .then((response: IChat[]) => {
        dispatch(fetchAllChatsSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchAllChatsError(error));
      });
  };

  const submitForm = React.useCallback((message: string): void => {
    chatApis
      .postNewChat({ payload: { message, author: 'Misa Qarahqani' } })
      .then((response: IChat) => {
        dispatch(postNewMessageSuccess(response));
      })
      // only for test as dummy data. because tokens are not valid :(
      .catch((error) =>
        dispatch(postNewMessageSuccess({ message, author: 'Misa Qarahqani', createdAt: Date.now(), owner: true })),
      );
  }, []);

  React.useEffect(() => {
    fetchAllChats({ token });
  }, []);

  React.useEffect(() => {
    const interval = setInterval(() => {
      fetchAllChats({ since, limit, token });
    }, intervalTime);
    return () => clearInterval(interval);
  }, []);

  return (
    <div className="chat-layout-container">
      <ChatList chats={state.chats} />
      <NewChat submitForm={submitForm} />
    </div>
  );
};

export default ChatLayout;
