import React from 'react';

const ChatList = ({ chats }): JSX.Element => {
  const bottomRef = React.useRef(null);

  React.useEffect(() => {
    bottomRef.current?.scrollIntoView({ behavior: 'smooth' });
  }, [chats]);

  return (
    <div className="chat-list-container" data-testid="chat-list-test-id">
      {chats.map((item) => (
        <div
          key={item.message}
          className={`chat-item flex-display flex-direction-column ${item.owner && ' owner-item align-self-end'}`}
        >
          <h4 className="color-gray">{item.author}</h4>
          <span className="font-size-20">{item.message}</span>
          <p className={`color-gray ${item.owner && 'align-self-end'}`}>
            <b>{new Date(item.createdAt).toLocaleString()}</b>
          </p>
        </div>
      ))}
      <div ref={bottomRef} />
    </div>
  );
};

export default ChatList;
