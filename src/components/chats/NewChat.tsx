import React from 'react';
import { SEND, MESSAGE } from 'constants/texts';

type Props = {
  submitForm: (message: string) => void;
};

const NewChat = ({ submitForm }: Props): JSX.Element => {
  const [message, setMessage] = React.useState<string>('');

  const handleOnChange = React.useCallback((e: React.FormEvent<HTMLInputElement>): void => {
    setMessage(e.currentTarget.value);
  }, []);

  const handleOnSubmitForm = (e: React.SyntheticEvent): void => {
    e.preventDefault();
    submitForm(message);
    setMessage('');
  };

  return (
    <div className="new-message-container">
      <form className="message-wrapper" onSubmit={handleOnSubmitForm}>
        <input
          required
          placeholder={MESSAGE}
          className="message-input"
          onChange={handleOnChange}
          value={message}
        ></input>
        <button className="submit-btn" type="submit">
          {SEND}
        </button>
      </form>
    </div>
  );
};

export default NewChat;
