// actions
import { Action, ChatAction } from 'actions/chatActions';
// types
import { IChat } from 'types/chat';

export const since = 1521096352339;
export const limit = 10;
export const intervalTime = 180000;

export interface State {
  chats: IChat[];
}

export const initialState: State = {
  chats: [
    {
      author: 'Nasrin Ariafar',
      message: 'hey guys, glad to join u and happy with u',
      createdAt: 1671306048547,
      owner: false,
    },
    {
      author: 'Shiva',
      message:
        'I really enjoyed.We understand your time is precious and would not want you to spend more than 3 to 5 hours on this over the span of one week max. The outcome should be runnable locally on a UNIX-flavored OS (MacOS, Linux) in a common browser.\n' +
        '\n' +
        'You must use JavaScript (Vanilla JS, React, ...). We want you to provide a responsive implementation. Keep in mind that Doodle is used worldwide and has to work on commonly used browsers.\n' +
        '\n' +
        'We expect to hear back from you in one week from now, latest.',
      createdAt: 1671306762752,
      owner: false,
    },
    {
      author: 'Misa',
      message:
        'Hey u :) . For this, we have prepared a simple API which receives new messages in a POST endpoint and lists all messages reverse chronological order in a GET endpoint. Please use your personal YOUR_AWESOME_UNIQUE_TOKEN.',
      createdAt: Date.now(),
      owner: true,
    },
    {
      author: 'Gan2mi',
      message: 'i hope that this group help us to improve our knowlege and community ',
      createdAt: 1671306048547,
      owner: false,
    },
    { author: 'tina', message: 'good luck', createdAt: 1671306048547, owner: false },
  ],
};

export function reducer(state: State, action: Action) {
  switch (action.type) {
    case ChatAction.GET_ALL_CHATS_REQUEST:
      return {
        ...state,
        chats: state.chats,
      };
    case ChatAction.GET_ALL_CHATS_SUCCESS:
      return {
        ...state,
        chats: initialState.chats,
      };
    case ChatAction.POST_NEW_CHAT_SUCCESS:
      return {
        ...state,
        chats: [...state.chats, action.payload],
      };
    default: {
      return state;
    }
  }
}
