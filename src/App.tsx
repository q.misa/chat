import React from 'react';
import 'styles/custom.css';
import ChatLayout from 'components/layouts/ChatLayout';

function App() {
  return <ChatLayout />;
}

export default App;
