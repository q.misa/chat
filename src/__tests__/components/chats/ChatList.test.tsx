import { render, screen } from '@testing-library/react';
import ChatList from 'components/chats/ChatList';

describe('Chat List', () => {
  test('renders ChatList', () => {
    window.HTMLElement.prototype.scrollIntoView = jest.fn();
    render(<ChatList chats={[]} />);
    expect(screen.getByTestId('chat-list-test-id')).toBeInTheDocument();
  });
});
