import { render, screen } from '@testing-library/react';
import NewChat from 'components/chats/NewChat';
import { SEND, MESSAGE } from 'constants/texts';

describe('New Chat', () => {
  const submitForm = jest.fn();
  test('renders NewChat', () => {
    render(<NewChat submitForm={submitForm} />);
    expect(screen.getByText(SEND)).toBeInTheDocument();
    expect(screen.getByPlaceholderText(MESSAGE)).toBeInTheDocument();
  });
});
