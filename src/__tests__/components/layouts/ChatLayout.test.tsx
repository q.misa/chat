import { render, screen } from '@testing-library/react';
import ChatLayout from 'components/layouts/ChatLayout';

describe('Chat Layout', () => {
  test('renders ChatLayout', () => {
    window.HTMLElement.prototype.scrollIntoView = jest.fn();
    render(<ChatLayout />);
    expect(screen.getByRole('button')).toBeInTheDocument();
  });
});
